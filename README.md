# Problema Día del Programador

La idea de celebrar el día del programador se les ocurrió a Valentin Balt y Michael Cherviakov, dos programadores rusos alrededor del año 2002, quienes consiguieron que fuera reconocido oficialmente por el gobierno ruso en 2009. El 11 de septiembre de 2009 el presidente de Rusia, Dmitry Medvedev firmó el decreto que oficializó esta celebración.

El día del programador corresponde al día 256 del año. Este número fue elegido porque es la cantidad de valores que se pueden representar en un byte de 8 bits, una cifra muy reconocible por los programadores y por supuesto es un valor menor a 365, que son los días del año.

1. Escribe una funcion que reciba como argumento el año.

Al ser llamada debe retornar una lista compuesta de los siguientes elementos en ese orden:
- el día del mes en que se celebrará el día del programador,
- el nombre del mes en que se celebrará el día del programador,
- el año que se introdujo como entrada,
- el nombre del día de la semana en que se celebrará el día del programador,
- y la palabra `"celebrará"`.

Por ejemplo si ejecuto la función con el parámetro 2025, el programa debe retornar `[13, "septiembre", 2021, "sábado", "celebrará"]`, con lo que yo podría formar la frase: _El día del programador del año 2025 se celebrará el sábado 13 de septiembre_.

2. El programa debe calcular cuál es el día 256 del año, es más, esto debe ser un parámetro interno y debería poder modificarse, de modo que si decidimos que el día del programador fuera el día 128 o el día 48 del año esto pueda modificarse fácilmente. (Esto es para evitar que calculen a secas el día del programador como 12 ó 13 de septiembre según si el año es bisiesto).

3. Si el año es menor a 2002 la lista que retorna la función sólo tendrá un elemento, una cadena de texto con el mensaje _Ese año aún no se celebraba el día del programador_.

4. Si el año ingresado está en el pasado, el último elemento de la lista, la palabra, debe estar en pretérito (para que yo pueda armar la frase, por ejemplo: _El día del programador del año 2014 se celebró el sábado 13 de septiembre_).

5. Si ejecutas el programa el mismo día del programador, la función debería retornar una lista con un solo elemento, la cadena de texto con el mensaje _¡El día del programador se celebra hoy! ¡Felicidades!_.
 

El problema es agnóstico de lenguaje, usa el que se te indique en el issue que se te asigne. O libremente si no se da ninguna indicación

## INSTRUCCIONES:

[Sustituye esta línea con las indicaciones para ejecutar tu programa (lenguaje, ambiente, versiones, comandos, etc.)]
